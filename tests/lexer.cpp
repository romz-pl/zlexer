#include <gtest/gtest.h>
#include "lexer.h"
#include <fstream>

TEST( lexer, punktactors )
{
    const std::string path = "./lexer.punktactors";
    std::ofstream out( path );
    out << "  <=   << ";
    out << " >=   >>  ";
    out << "->   --  ";
    out << " ==  = ";
    out << " !=  ! ";
    out << " ||  | ";
    out << " &&  & ";
    out << " ++  + ";

    out.close();

    lexer lex{ std::string( path ) };

    EXPECT_TRUE( lex.get_token() == token_kind::LESSEQUAL );
    EXPECT_TRUE( lex.get_token() == token_kind::LESSLESS );

    EXPECT_TRUE( lex.get_token() == token_kind::GREATEREQUAL );
    EXPECT_TRUE( lex.get_token() == token_kind::GREATERGREATER );

    EXPECT_TRUE( lex.get_token() == token_kind::ARROW );
    EXPECT_TRUE( lex.get_token() == token_kind::MINUSMINUS );

    EXPECT_TRUE( lex.get_token() == token_kind::EQUALEQUAL );
    EXPECT_TRUE( lex.get_token() == token_kind::EQUAL );

    EXPECT_TRUE( lex.get_token() == token_kind::EXCLAIMEQUAL );
    EXPECT_TRUE( lex.get_token() == token_kind::EXCLAIM );

    EXPECT_TRUE( lex.get_token() == token_kind::PIPEPIPE );
    EXPECT_TRUE( lex.get_token() == token_kind::PIPE );

    EXPECT_TRUE( lex.get_token() == token_kind::AMPAMP );
    EXPECT_TRUE( lex.get_token() == token_kind::AMP );

    EXPECT_TRUE( lex.get_token() == token_kind::PLUSPLUS );
    EXPECT_TRUE( lex.get_token() == token_kind::PLUS );
}

TEST( lexer, identyfier )
{
    const std::string path = "./lexer.identyfier";

    std::ofstream out( path );
    out << " identyfier ";
    out << " hDeNtyFier ";
    out << " ja ";
    out << " kb ";
    out << " mbcd ";
    out << " if  ";
    out << " int ";

    out.close();

    lexer lex{ std::string( path ) };

    EXPECT_TRUE( lex.get_token() == token_kind::IDENTIFIER );
    EXPECT_TRUE( lex.get_token() == token_kind::IDENTIFIER );
    EXPECT_TRUE( lex.get_token() == token_kind::IDENTIFIER );
    EXPECT_TRUE( lex.get_token() == token_kind::IDENTIFIER );
    EXPECT_TRUE( lex.get_token() == token_kind::IDENTIFIER );

    EXPECT_TRUE( lex.get_token() == token_kind::IF );
    EXPECT_TRUE( lex.get_token() == token_kind::INT );
}

TEST( lexer, keyword )
{
    const std::string path = "./lexer.keyword";

    std::ofstream out( path );

    out << " auto ";
    out << " break ";
    out << " case ";
    out << " char ";
    out << " const ";
    out << " continue ";
    out << " default ";
    out << " do ";
    out << " double ";
    out << " else ";
    out << " enum ";
    out << " extern ";
    out << " float ";
    out << " for ";
    out << " goto ";
    out << " if ";
    out << " inline ";
    out << " int ";
    out << " long ";
    out << " register ";
    out << " restrict ";
    out << " return ";
    out << " short ";
    out << " signed ";
    out << " sizeof ";
    out << " static ";
    out << " struct ";
    out << " switch ";
    out << " typedef ";
    out << " union ";
    out << " unsigned ";
    out << " void ";
    out << " volatile ";
    out << " while ";

    out.close();

    lexer lex{ std::string( path ) };

    EXPECT_TRUE( lex.get_token() == token_kind::AUTO );
    EXPECT_TRUE( lex.get_token() == token_kind::BREAK );
    EXPECT_TRUE( lex.get_token() == token_kind::CASE );
    EXPECT_TRUE( lex.get_token() == token_kind::CHAR );
    EXPECT_TRUE( lex.get_token() == token_kind::CONST );
    EXPECT_TRUE( lex.get_token() == token_kind::CONTINUE );
    EXPECT_TRUE( lex.get_token() == token_kind::DEFAULT );
    EXPECT_TRUE( lex.get_token() == token_kind::DO );
    EXPECT_TRUE( lex.get_token() == token_kind::DOUBLE );
    EXPECT_TRUE( lex.get_token() == token_kind::ELSE );
    EXPECT_TRUE( lex.get_token() == token_kind::ENUM );
    EXPECT_TRUE( lex.get_token() == token_kind::EXTERN );
    EXPECT_TRUE( lex.get_token() == token_kind::FLOAT );
    EXPECT_TRUE( lex.get_token() == token_kind::FOR );
    EXPECT_TRUE( lex.get_token() == token_kind::GOTO );
    EXPECT_TRUE( lex.get_token() == token_kind::IF );
    EXPECT_TRUE( lex.get_token() == token_kind::INLINE );
    EXPECT_TRUE( lex.get_token() == token_kind::INT );
    EXPECT_TRUE( lex.get_token() == token_kind::LONG );
    EXPECT_TRUE( lex.get_token() == token_kind::REGISTER );
    EXPECT_TRUE( lex.get_token() == token_kind::RESTRICT );
    EXPECT_TRUE( lex.get_token() == token_kind::RETURN );
    EXPECT_TRUE( lex.get_token() == token_kind::SHORT );
    EXPECT_TRUE( lex.get_token() == token_kind::SIGNED );
    EXPECT_TRUE( lex.get_token() == token_kind::SIZEOF );
    EXPECT_TRUE( lex.get_token() == token_kind::STATIC );
    EXPECT_TRUE( lex.get_token() == token_kind::STRUCT );
    EXPECT_TRUE( lex.get_token() == token_kind::SWITCH );
    EXPECT_TRUE( lex.get_token() == token_kind::TYPEDEF );
    EXPECT_TRUE( lex.get_token() == token_kind::UNION );
    EXPECT_TRUE( lex.get_token() == token_kind::UNSIGNED );
    EXPECT_TRUE( lex.get_token() == token_kind::VOID );
    EXPECT_TRUE( lex.get_token() == token_kind::VOLATILE );
    EXPECT_TRUE( lex.get_token() == token_kind::WHILE );
}

TEST( lexer, FLOATING_CONSTANT )
{
    const std::string path = "./lexer.keyword";

    std::ofstream out( path );

    out << " .01234 ";
    out << " .01e-3 ";
    out << " .01E-3 ";
    out << " .01e+3 ";
    out << " .01E+3 ";


    out.close();

    lexer lex{ std::string( path ) };

    EXPECT_TRUE( lex.get_token() == token_kind::FLOATING_CONSTANT );
    EXPECT_TRUE( lex.get_token() == token_kind::FLOATING_CONSTANT );
    EXPECT_TRUE( lex.get_token() == token_kind::FLOATING_CONSTANT );
    EXPECT_TRUE( lex.get_token() == token_kind::FLOATING_CONSTANT );
    EXPECT_TRUE( lex.get_token() == token_kind::FLOATING_CONSTANT );

}


