#pragma once

#include "character_map.h"
#include <string>
#include <vector>

class input
{
public:
    input( const std::string& path );

    uint8_t get();
    std::string get_identyfier();
    std::string get_FLOATING_CONSTANT();

    bool is_next( uint8_t c );

    void skip_white_space();

    bool is_keyword( const std::string& keyword );

    bool is_end() const;

    bool is_ELLIPSIS();
    bool is_digit() const;


private:
    std::vector< uint8_t > m_buf;

    size_t m_cp;

    character_map m_map;
};
