#include "character_map.h"

std::array< character_type, 256 > character_map::m_map;

character_map::character_map()
{
    m_map.fill( character_type::UNSUPPORTED );

    init_new_line();
    init_blank();
    init_other();
    init_digit();
    init_letter();
    init_letter_or_hex();
}

bool character_map::is_new_line( uint8_t c ) const
{
    return m_map[ c ] == character_type::NEWLINE;
}

bool character_map::is_blank( uint8_t c ) const
{
    return m_map[ c ] == character_type::BLANK;
}

bool character_map::is_other( uint8_t c ) const
{
    return m_map[ c ] == character_type::OTHER;
}

bool character_map::is_digit( uint8_t c ) const
{
    return m_map[ c ] == character_type::DIGIT;
}

bool character_map::is_letter( uint8_t c ) const
{
    return m_map[ c ] == character_type::LETTER;
}

bool character_map::is_digit_or_letter( uint8_t c ) const
{
    return is_digit( c ) || is_letter( c );
}

void character_map::init_new_line()
{
    m_map[ '\n' ] = character_type::NEWLINE;
}

void character_map::init_blank()
{
    m_map[ '\v' ] = character_type::BLANK;
    m_map[ '\r' ] = character_type::BLANK;
    m_map[ '\f' ] = character_type::BLANK;
    m_map[ ' '  ] = character_type::BLANK;
}

void character_map::init_other()
{
    m_map[ '!' ] = character_type::OTHER;
    m_map[ '"' ] = character_type::OTHER;
    m_map[ '#' ] = character_type::OTHER;

    m_map[ '%' ] = character_type::OTHER;
    m_map[ '&' ] = character_type::OTHER;
    m_map[ '\'' ] = character_type::OTHER;

    m_map[ '(' ] = character_type::OTHER;
    m_map[ ')' ] = character_type::OTHER;
    m_map[ '*' ] = character_type::OTHER;
    m_map[ '+' ] = character_type::OTHER;
    m_map[ ',' ] = character_type::OTHER;
    m_map[ '-' ] = character_type::OTHER;
    m_map[ '.' ] = character_type::OTHER;
    m_map[ '/' ] = character_type::OTHER;

    m_map[ ':' ] = character_type::OTHER;
    m_map[ ';' ] = character_type::OTHER;
    m_map[ '<' ] = character_type::OTHER;
    m_map[ '=' ] = character_type::OTHER;
    m_map[ '>' ] = character_type::OTHER;
    m_map[ '?' ] = character_type::OTHER;

    m_map[ '[' ] = character_type::OTHER;
    m_map[ '\\' ] = character_type::OTHER;
    m_map[ ']' ] = character_type::OTHER;
    m_map[ '^' ] = character_type::OTHER;

    m_map[ '{' ] = character_type::OTHER;
    m_map[ '|' ] = character_type::OTHER;
    m_map[ '}' ] = character_type::OTHER;
    m_map[ '~' ] = character_type::OTHER;
}

void character_map::init_digit()
{
    m_map[ '0' ] = character_type::DIGIT;
    m_map[ '1' ] = character_type::DIGIT;
    m_map[ '2' ] = character_type::DIGIT;
    m_map[ '3' ] = character_type::DIGIT;
    m_map[ '4' ] = character_type::DIGIT;
    m_map[ '5' ] = character_type::DIGIT;
    m_map[ '6' ] = character_type::DIGIT;
    m_map[ '7' ] = character_type::DIGIT;
    m_map[ '8' ] = character_type::DIGIT;
    m_map[ '9' ] = character_type::DIGIT;
}

void character_map::init_letter()
{
    m_map[ 'A' ] = character_type::LETTER;
    m_map[ 'B' ] = character_type::LETTER;
    m_map[ 'C' ] = character_type::LETTER;
    m_map[ 'D' ] = character_type::LETTER;
    m_map[ 'E' ] = character_type::LETTER;
    m_map[ 'F' ] = character_type::LETTER;
    m_map[ 'G' ] = character_type::LETTER;
    m_map[ 'H' ] = character_type::LETTER;
    m_map[ 'I' ] = character_type::LETTER;
    m_map[ 'J' ] = character_type::LETTER;
    m_map[ 'K' ] = character_type::LETTER;
    m_map[ 'L' ] = character_type::LETTER;
    m_map[ 'M' ] = character_type::LETTER;
    m_map[ 'N' ] = character_type::LETTER;
    m_map[ 'O' ] = character_type::LETTER;
    m_map[ 'P' ] = character_type::LETTER;
    m_map[ 'Q' ] = character_type::LETTER;
    m_map[ 'R' ] = character_type::LETTER;
    m_map[ 'S' ] = character_type::LETTER;
    m_map[ 'T' ] = character_type::LETTER;
    m_map[ 'U' ] = character_type::LETTER;
    m_map[ 'V' ] = character_type::LETTER;
    m_map[ 'W' ] = character_type::LETTER;
    m_map[ 'X' ] = character_type::LETTER;
    m_map[ 'Y' ] = character_type::LETTER;
    m_map[ 'Z' ] = character_type::LETTER;

    m_map[ 'a' ] = character_type::LETTER;
    m_map[ 'b' ] = character_type::LETTER;
    m_map[ 'c' ] = character_type::LETTER;
    m_map[ 'd' ] = character_type::LETTER;
    m_map[ 'e' ] = character_type::LETTER;
    m_map[ 'f' ] = character_type::LETTER;
    m_map[ 'g' ] = character_type::LETTER;
    m_map[ 'h' ] = character_type::LETTER;
    m_map[ 'i' ] = character_type::LETTER;
    m_map[ 'j' ] = character_type::LETTER;
    m_map[ 'k' ] = character_type::LETTER;
    m_map[ 'l' ] = character_type::LETTER;
    m_map[ 'm' ] = character_type::LETTER;
    m_map[ 'n' ] = character_type::LETTER;
    m_map[ 'o' ] = character_type::LETTER;
    m_map[ 'p' ] = character_type::LETTER;
    m_map[ 'q' ] = character_type::LETTER;
    m_map[ 'r' ] = character_type::LETTER;
    m_map[ 's' ] = character_type::LETTER;
    m_map[ 't' ] = character_type::LETTER;
    m_map[ 'u' ] = character_type::LETTER;
    m_map[ 'v' ] = character_type::LETTER;
    m_map[ 'w' ] = character_type::LETTER;
    m_map[ 'x' ] = character_type::LETTER;
    m_map[ 'y' ] = character_type::LETTER;
    m_map[ 'z' ] = character_type::LETTER;

    m_map[ '_' ] = character_type::LETTER;

}

void character_map::init_letter_or_hex()
{



}
