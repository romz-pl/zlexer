#include "input.h"
#include <fstream>
#include <cassert>


input::input( const std::string& path )
{
    std::ifstream in( path, std::ios::binary | std::ios::ate );
    if( !in.is_open() )
    {
        throw "Cannot open file " + path;
    }

    const auto size = in.tellg();
    m_buf.resize( static_cast< size_t>( size ) );
    in.seekg( 0 );
    if( !in.read( reinterpret_cast< char* >( &m_buf[ 0 ] ), size ) )
    {
        throw "Cannot read file " + path;
    }

    if( m_buf.empty() )
    {
        throw "Empty buffer";
    }

    m_cp = 0;

}

bool input::is_end() const
{
    return ( m_cp >= m_buf.size() );
}

uint8_t input::get()
{
    if( m_cp >= m_buf.size() )
    {
        throw "Out of input buffer";
    }

    const uint8_t v = m_buf[ m_cp ];
    m_cp++;
    return v;
}

std::string input::get_identyfier( )
{
    std::string ret;
    while( true )
    {
        const uint8_t v = m_buf[ m_cp ];
        if( m_map.is_digit_or_letter( v ) )
        {
            m_cp++;
            ret += static_cast< char >( m_buf[ m_cp ] );
        }
        else
            return ret;
    }
}

std::string input::get_FLOATING_CONSTANT()
{
    std::string ret(".");
    while( true )
    {
        const uint8_t v = m_buf[ m_cp ];
        if( m_map.is_digit( v ) )
        {
            ret += static_cast< char >( m_buf[ m_cp ] );
            m_cp++;
        }
        else
            break;
    }

    if (m_buf[ m_cp ] == 'e' || m_buf[ m_cp ] == 'E')
    {
        ret += static_cast< char >( m_buf[ m_cp ] );
        m_cp++;
        if (m_buf[ m_cp ] == '-' || m_buf[ m_cp ] == '+')
        {
            ret += static_cast< char >( m_buf[ m_cp ] );
            m_cp++;
        }
        const uint8_t w = m_buf[ m_cp ];
        if( !m_map.is_digit( w ) )
        {
            throw "Invalid floating constant";
        }

        while( true )
        {
            const uint8_t v = m_buf[ m_cp ];
            if( m_map.is_digit( v ) )
            {
                ret += static_cast< char >( m_buf[ m_cp ] );
                m_cp++;
            }
            else
                break;
        }

    }

    return ret;
}

bool input::is_next( uint8_t c )
{
    if( m_buf[ m_cp ] == c )
    {
        m_cp++;
        return true;
    }
    return false;

}

bool input::is_ELLIPSIS()
{
    if( m_buf[ m_cp ] == '.' && m_buf[ m_cp + 1 ] == '.' )
    {
        m_cp += 2;
        return true;
    }
    return false;
}

bool input::is_digit() const
{
    return m_map.is_digit( m_buf[ m_cp ] );
}

bool input::is_keyword( const std::string& keyword )
{
    assert( m_cp >= 1 );
    size_t k = m_cp - 1;
    for( char c : keyword )
    {
        if( c != m_buf[ k ] )
            return false;
        k++;
    }

    const uint8_t v = m_buf[ m_cp + keyword.size() - 1 ];
    if( !m_map.is_digit_or_letter( v ) )
    {
        m_cp += keyword.size() - 1;
        return true;
    }
    return false;
}

