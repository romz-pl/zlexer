#pragma once

#include "character_type.h"
#include <array>

///
/// Lexer recognizes a token by switching on its first character, which
/// classifies the tocken, and consuming subcequent characters that makes us the token.
/// For some tokens, these characters are given by one or more of the sets
/// defined by character_map.
///
/// character_map[ c ] is the mask that classifies chcracter `c` as
/// a member of one ore more of six character types defined by enum character_type
///
class character_map
{
public:
    character_map();

    bool is_new_line( uint8_t c ) const;
    bool is_blank( uint8_t c ) const;
    bool is_other( uint8_t c ) const;
    bool is_digit( uint8_t c ) const;
    bool is_letter( uint8_t c ) const;

    bool is_digit_or_letter( uint8_t c ) const;


private:
    void init_new_line();
    void init_blank();
    void init_other();
    void init_digit();
    void init_letter();
    void init_letter_or_hex();

private:

    // 256 == 2^8
    static std::array< character_type, 256 > m_map;
};
