#include "lexer.h"
#include "character_map.h"
#include <fstream>

lexer::lexer( const std::string& path ) : m_input( path )
{
}



token_kind lexer::get_token()
{
    for (;;)
    {
        const uint8_t c = m_input.get();

        if( c == ' ' || c == '\n' || c == '\v' || c == '\r' || c == '\f' || c == '\t' )
            continue;

        if( c >= 'A' && c <= 'Z' )
            return scan_capital_letter( c );

        if( c >= 'a' && c <= 'z' )
            return scan_small_letter( c );


        return scan_punctuator( c );
    }

}

token_kind lexer::scan_capital_letter( uint8_t c )
{
    switch( c )
    {
        case 'A': return scan_identyfier();
        case 'B': return scan_identyfier();
        case 'C': return scan_identyfier();
        case 'D': return scan_identyfier();
        case 'E': return scan_identyfier();
        case 'F': return scan_identyfier();
        case 'G': return scan_identyfier();
        case 'H': return scan_identyfier();
        case 'I': return scan_identyfier();
        case 'J': return scan_identyfier();
        case 'K': return scan_identyfier();
        case 'M': return scan_identyfier();
        case 'N': return scan_identyfier();
        case 'O': return scan_identyfier();
        case 'P': return scan_identyfier();
        case 'Q': return scan_identyfier();
        case 'R': return scan_identyfier();
        case 'S': return scan_identyfier();
        case 'T': return scan_identyfier();
        case 'U': return scan_identyfier();
        case 'V': return scan_identyfier();
        case 'W': return scan_identyfier();
        case 'X': return scan_identyfier();
        case 'Y': return scan_identyfier();
        case 'Z': return scan_identyfier();

        default:
            throw "Invalid character";
    }
}

token_kind lexer::scan_small_letter( uint8_t c )
{
    switch( c )
    {
        case 'a': return scan_a();
        case 'b': return scan_b();
        case 'c': return scan_c();
        case 'd': return scan_d();
        case 'e': return scan_e();
        case 'f': return scan_f();
        case 'g': return scan_g();
        case 'h': return scan_identyfier();
        case 'i': return scan_i();
        case 'j': return scan_identyfier();
        case 'k': return scan_identyfier();
        case 'l': return scan_l();
        case 'm': return scan_identyfier();
        case 'n': return scan_identyfier();
        case 'o': return scan_identyfier();
        case 'p': return scan_identyfier();
        case 'q': return scan_identyfier();
        case 'r': return scan_r();
        case 's': return scan_s();
        case 't': return scan_t();
        case 'u': return scan_u();
        case 'v': return scan_v();
        case 'w': return scan_w();
        case 'x': return scan_identyfier();
        case 'y': return scan_identyfier();
        case 'z': return scan_identyfier();


        default:
            throw "Invalid character";

    }
}

token_kind lexer::scan_a()
{
    if (m_input.is_keyword( "auto" ))
        return token_kind::AUTO;

    m_input.get_identyfier();
    return token_kind::IDENTIFIER;
}

token_kind lexer::scan_b()
{
    if (m_input.is_keyword( "break" ))
        return token_kind::BREAK;

    m_input.get_identyfier();
    return token_kind::IDENTIFIER;
}

token_kind lexer::scan_c()
{
    if (m_input.is_keyword( "case" ))
        return token_kind::CASE;

    if (m_input.is_keyword( "char" ))
        return token_kind::CHAR;

    if (m_input.is_keyword( "const" ))
        return token_kind::CONST;

    if (m_input.is_keyword( "continue" ))
        return token_kind::CONTINUE;

    m_input.get_identyfier();
    return token_kind::IDENTIFIER;
}

token_kind lexer::scan_d()
{
    if (m_input.is_keyword( "default" ))
        return token_kind::DEFAULT;

    if (m_input.is_keyword( "double" ))
        return token_kind::DOUBLE;

    if (m_input.is_keyword( "do" ))
        return token_kind::DO;

    m_input.get_identyfier();
    return token_kind::IDENTIFIER;
}

token_kind lexer::scan_e()
{
    if (m_input.is_keyword( "else" ))
        return token_kind::ELSE;

    if (m_input.is_keyword( "enum" ))
        return token_kind::ENUM;

    if (m_input.is_keyword( "extern" ))
        return token_kind::EXTERN;

    m_input.get_identyfier();
    return token_kind::IDENTIFIER;
}

token_kind lexer::scan_f()
{
    if (m_input.is_keyword( "float" ))
        return token_kind::FLOAT;

    if (m_input.is_keyword( "for" ))
        return token_kind::FOR;

    m_input.get_identyfier();
    return token_kind::IDENTIFIER;
}

token_kind lexer::scan_g()
{
    if (m_input.is_keyword( "goto" ))
        return token_kind::GOTO;

    m_input.get_identyfier();
    return token_kind::IDENTIFIER;
}

token_kind lexer::scan_i()
{
    if (m_input.is_keyword( "if" ))
        return token_kind::IF;

    if (m_input.is_keyword( "int" ))
        return token_kind::INT;

    if (m_input.is_keyword( "inline" ))
        return token_kind::INLINE;

    m_input.get_identyfier();
    return token_kind::IDENTIFIER;
}

token_kind lexer::scan_l()
{
    if (m_input.is_keyword( "long" ))
        return token_kind::LONG;

    m_input.get_identyfier();
    return token_kind::IDENTIFIER;
}

token_kind lexer::scan_r()
{
    if (m_input.is_keyword( "register" ))
        return token_kind::REGISTER;

    if (m_input.is_keyword( "return" ))
        return token_kind::RETURN;

    if (m_input.is_keyword( "restrict" ))
        return token_kind::RESTRICT;

    m_input.get_identyfier();
    return token_kind::IDENTIFIER;
}

token_kind lexer::scan_s()
{
    if (m_input.is_keyword( "short" ))
        return token_kind::SHORT;

    if (m_input.is_keyword( "signed" ))
        return token_kind::SIGNED;

    if (m_input.is_keyword( "sizeof" ))
        return token_kind::SIZEOF;

    if (m_input.is_keyword( "static" ))
        return token_kind::STATIC;

    if (m_input.is_keyword( "struct" ))
        return token_kind::STRUCT;

    if (m_input.is_keyword( "switch" ))
        return token_kind::SWITCH;

    m_input.get_identyfier();
    return token_kind::IDENTIFIER;
}

token_kind lexer::scan_t()
{
    if (m_input.is_keyword( "typedef" ))
        return token_kind::TYPEDEF;

    m_input.get_identyfier();
    return token_kind::IDENTIFIER;
}

token_kind lexer::scan_u()
{
    if (m_input.is_keyword( "union" ))
        return token_kind::UNION;

    if (m_input.is_keyword( "unsigned" ))
        return token_kind::UNSIGNED;

    m_input.get_identyfier();
    return token_kind::IDENTIFIER;
}

token_kind lexer::scan_v()
{
    if (m_input.is_keyword( "void" ))
        return token_kind::VOID;

    if (m_input.is_keyword( "volatile" ))
        return token_kind::VOLATILE;

    m_input.get_identyfier();
    return token_kind::IDENTIFIER;
}

token_kind lexer::scan_w()
{
    if (m_input.is_keyword( "while" ))
        return token_kind::WHILE;

    m_input.get_identyfier();
    return token_kind::IDENTIFIER;
}

token_kind lexer::scan_identyfier()
{
    m_input.get_identyfier();
    return token_kind::IDENTIFIER;
}


token_kind lexer::scan_punctuator( uint8_t c )
{
    switch( c )
    {
        case '<': return scan_LESS();
        case '>': return scan_GREATER();
        case '-': return scan_MINUS();
        case '=': return scan_EQUAL();
        case '!': return scan_EXCLAIM();
        case '|': return scan_PIPE();
        case '&': return scan_AMP();
        case '+': return scan_PLUS();
        case '"': return scan_STRING_LITERAL();
        case '.': return scan_PERIOD();

        case ';': return token_kind::SEMI;
        case ',': return token_kind::COMMA;
        case ':': return token_kind::COLON;
        case '*': return token_kind::STAR;
        case '~': return token_kind::TILDE;
        case '%': return token_kind::PERCENT;
        case '^': return token_kind::CARET;
        case '?': return token_kind::QUESTION;
        case '[': return token_kind::L_SQUARE;
        case ']': return token_kind::R_SQUARE;
        case '{': return token_kind::L_BRACE;
        case '}': return token_kind::R_BRACE;
        case '(': return token_kind::L_PAREN;
        case ')': return token_kind::R_PAREN;


        default: return token_kind::UNKNOWN;
    }
}

token_kind lexer::scan_PERIOD()
{
    if (m_input.is_ELLIPSIS( ))
        return token_kind::ELLIPSIS;

    if ( !m_input.is_digit( ) )
        return token_kind::PERIOD;

    m_input.get_FLOATING_CONSTANT();
    return token_kind::FLOATING_CONSTANT;

}

token_kind lexer::scan_STRING_LITERAL()
{
    std::string str;
    while( true )
    {
        const uint8_t c = m_input.get();

        if( m_input.is_end() )
            throw "String literal has no closing \" sign";

        if( c == '"' )
            break;

        if( c == '\n' )
            throw "String literal cannot span multiple lines";

        str += static_cast< char >( c );
    }

    return token_kind::STRING_LITERAL;
}

token_kind lexer::scan_LESS()
{
    if (m_input.is_next( '=' ))
        return token_kind::LESSEQUAL;

    if (m_input.is_next( '<' ))
        return token_kind::LESSLESS;

    return token_kind::LESS;
}

token_kind lexer::scan_GREATER()
{
    if (m_input.is_next( '=' ))
        return token_kind::GREATEREQUAL;

    if (m_input.is_next( '>' ))
        return token_kind::GREATERGREATER;

    return token_kind::GREATER;
}

token_kind lexer::scan_MINUS()
{
    if (m_input.is_next( '>' ))
        return token_kind::ARROW;

    if (m_input.is_next( '-' ))
        return token_kind::MINUSMINUS;

    return token_kind::MINUS;
}

token_kind lexer::scan_EQUAL()
{
    if (m_input.is_next( '=' ))
        return token_kind::EQUALEQUAL;

    return token_kind::EQUAL;
}

token_kind lexer::scan_EXCLAIM()
{
    if (m_input.is_next( '=' ))
        return token_kind::EXCLAIMEQUAL;

    return token_kind::EXCLAIM;
}

token_kind lexer::scan_PIPE()
{
    if (m_input.is_next( '|' ))
        return token_kind::PIPEPIPE;

    return token_kind::PIPE;
}

token_kind lexer::scan_AMP()
{
    if (m_input.is_next( '&' ))
        return token_kind::AMPAMP;

    return token_kind::AMP;
}

token_kind lexer::scan_PLUS()
{
    if (m_input.is_next( '+' ))
        return token_kind::PLUSPLUS;

    return token_kind::PLUS;
}
