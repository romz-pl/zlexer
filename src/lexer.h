#pragma once

#include "token_kind.h"
#include "character_map.h"
#include "input.h"
#include <string>
#include <vector>

class lexer
{
public:
    lexer( const std::string& path);

    token_kind get_token();


private:
    token_kind scan_capital_letter( uint8_t c );
    token_kind scan_small_letter( uint8_t c );
    token_kind scan_punctuator( uint8_t c );


    token_kind scan_LESS();
    token_kind scan_GREATER();
    token_kind scan_MINUS();
    token_kind scan_EQUAL();
    token_kind scan_EXCLAIM();
    token_kind scan_PIPE();
    token_kind scan_AMP();
    token_kind scan_PLUS();
    token_kind scan_PERIOD();
    token_kind scan_STRING_LITERAL();

    token_kind scan_a();
    token_kind scan_b();
    token_kind scan_c();
    token_kind scan_d();
    token_kind scan_e();
    token_kind scan_f();
    token_kind scan_g();
    token_kind scan_i();
    token_kind scan_l();
    token_kind scan_r();
    token_kind scan_s();
    token_kind scan_t();
    token_kind scan_u();
    token_kind scan_v();
    token_kind scan_w();

    token_kind scan_identyfier();




private:
    input m_input;

    character_map m_map;
};
