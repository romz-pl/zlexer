#pragma once

enum class character_type
{
    UNSUPPORTED = 0,
    BLANK = 01,
    NEWLINE = 02,
    LETTER = 04,
    DIGIT = 010,
    HEX = 020,
    OTHER = 040,
};
