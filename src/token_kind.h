#pragma once


enum class token_kind
{
    IDENTIFIER,          // ABCDE123
    INTEGER_CONSTANT,    // 123
    FLOATING_CONSTANT,   // 123.1E3
    CHAR_CONSTANT,       // 'A'
    WIDE_CHAR_CONSTANT,  // L'B'
    STRING_LITERAL,      // "FOO"
    WIDE_STRING_LITERAL, // L"FOO"
    L_SQUARE,            // "["
    R_SQUARE,            // "]"
    L_PAREN,             // "("
    R_PAREN,             // ")"
    L_BRACE,             // "{"
    R_BRACE,             // "}"
    PERIOD,              // "."
    ELLIPSIS,            // "..."
    AMP,                 // "&"
    AMPAMP,              // "&&"
    AMPEQUAL,            // "&="
    STAR,                // "*"
    STAREQUAL,           // "*="
    PLUS,                // "+"
    PLUSPLUS,            // "++"
    PLUSEQUAL,           // "+="
    MINUS,               // "-"
    ARROW,               // "->"
    MINUSMINUS,          // "--"
    MINUSEQUAL,          // "-="
    TILDE,               // "~"
    EXCLAIM,             // "!"
    EXCLAIMEQUAL,        // "!="
    SLASH,               // "/"
    SLASHEQUAL,          // "/="
    PERCENT,             // "%"
    PERCENTEQUAL,        // "%="
    LESS,                // "<"
    LESSLESS,            // "<<"
    LESSEQUAL,           // "<="
    LESSLESSEQUAL,       // "<<="
    SPACESHIP,           // "<=>"
    GREATER,             // ">"
    GREATERGREATER,      // ">>"
    GREATEREQUAL,        // ">="
    GREATERGREATEREQUAL, // ">>="
    CARET,               // "^"
    CARETEQUAL,          // "^="
    PIPE,                // "|"
    PIPEPIPE,            // "||"
    PIPEEQUAL,           // "|="
    QUESTION,            // "?"
    COLON,               // ":"
    SEMI,                // ";"
    EQUAL,               // "="
    EQUALEQUAL,          // "=="
    COMMA,               // ","
    AUTO,
    BREAK,
    CASE,
    CHAR,
    CONST,
    CONTINUE,
    DEFAULT,
    DO,
    DOUBLE,
    ELSE,
    ENUM,
    EXTERN,
    FLOAT,
    FOR,
    GOTO,
    IF,
    INLINE,
    INT,
    LONG,
    REGISTER,
    RESTRICT,
    RETURN,
    SHORT,
    SIGNED,
    SIZEOF,
    STATIC,
    STRUCT,
    SWITCH,
    TYPEDEF,
    UNION,
    UNSIGNED,
    VOID,
    VOLATILE,
    WHILE,
    UNKNOWN
};
